Document: solfege-doc-ru
Title: GNU Solfege Help (Russian)
Author: Tom Cato Amundsen
Abstract: Manual of the GNU solfege software.
Section: Education

Format: HTML
Index: /usr/share/doc/solfege/help/ru/index.html
Files: /usr/share/doc/solfege/help/ru/*.html
