Document: solfege-doc-nl
Title: GNU Solfege Help (Netherlands)
Author: Tom Cato Amundsen
Abstract: Manual of the GNU solfege software.
Section: Education

Format: HTML
Index: /usr/share/doc/solfege/help/nl/index.html
Files: /usr/share/doc/solfege/help/nl/*.html
