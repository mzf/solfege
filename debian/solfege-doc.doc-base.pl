Document: solfege-doc-pl
Title: GNU Solfege Help (Polish)
Author: Tom Cato Amundsen
Abstract: Manual of the GNU solfege software.
Section: Education

Format: HTML
Index: /usr/share/doc/solfege/help/pl/index.html
Files: /usr/share/doc/solfege/help/pl/*.html
